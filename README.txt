This package consists of two executables
- MidiSmoother
	- The main application
	- Uses a 2 Pole Chebshev Filter with min/max prefiltering
	- Filter updates regularly in its own thread at the same rate (DEFAULT)
		- there is a #define in USE_FILTERING_THREAD to disable this		
		- with the thread disabled, it updates upon calls to RequestMSToMoveValue 
- FilterGainCalculator
	- Test bed for filter gains used in MidiSmoother, which is the primary application
	- Simulates MidiSmoother in a non-real-time context to allow for latency estimation 
	- Tests multiple filters simultaneously, outputs results to a .csv file for analysis
		FIR (Finite Impulse Response)
			Moving Average Filter
		IIR (Infinite Impulse Response)
			Alpha Beta
			Alpha Beta Cascade (Length 3)
			Chebyshev (2 Pole)
			Chebyshev (4 Pole)
			Elliptic
			Chebyshev 2 Pole w/ average of min & max as prefilter
	- See FilterGainCalculator/Plots subfolder for plots of filtered waveforms

-------------------------------------------------------------------------------
USAGE FOR MidiSmoother
-------------------------------------------------------------------------------
cd MidiSmoother\MidiSmoother.VS

Debug\MidiSmoother.exe ../data/midi_data_sz.csv
Debug\MidiSmoother.exe ../data/midi_data_sz_scratch.csv
Debug\MidiSmoother.exe ../data/midi_data_sz_combined.csv

-------------------------------------------------------------------------------
USAGE FOR FilterGainCalculator
-------------------------------------------------------------------------------
cd MidiSmoother\FilterGainCalculator.VS
Debug\FilterGainCalculator.exe ".../data/midi_data_sz_combined.csv"

-------------------------------------------------------------------------------
Methodology: the entire length of the input and output were loaded into audacity, treated as a 44100hz mono audio files  and low pass filtered. The ideal cutoff was approximately 600hz. This corresponds to a 0.0136 frequency cutoff for an ideal low pass filter, however some of the filters have slow falloff in the stop band, so we shift them further left.

Filters were selected to have approx 20-24ms of latency when compared to the input signal, using simple comparison of input and output zero crossings. Each filter was tuned to a range of different input parameters. The selected parameters were chosen for smoothness while maintaining approximately the target latency. Ultimately, the preferred filter chosen was a two pole chebyshev filter with f_normalised = 0.0075 and 2% ripple.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
NOISE IN THE INPUT DATA IS PROPORTIONAL NOISE
-------------------------------------------------------------------------------
Using a simple high pass filter (taking velocity(t) – velocity(-1) i.e. essentially acceleration) illustrates that noise generally peaks with each trough and peak of the waveform. There are some exceptions. Perhaps filtering could be improved with the addition of some kind of derivative-based control

Perhaps an upper/lower bound system could help with smoothness, where we look for the highest and lower points within a rolling block and combine them with the filtered value

Noise is roughly proportional to the distance the platter has moved in the last time step,
which means noise magnitude is proportional to velocity.
This is true in both sets of example data, especially the spin down
This enables the use of zero crossing points as a reliable, if crude method for latency estimation,
as noise is minimal around zero crossings.

-------------------------------------------------------------------------------
DISCONTINUITIES: RATE OF VELOCITY READ VS RATE OF VELOCITY UPDATE
-------------------------------------------------------------------------------
Let f_midi be the average rate at which MIDI messages come in
L f_read be the rate at which the velocity value is read via RequestMSToMoveValue

In practice, f_midi is usually much lower than f_read
If we update velocity only when MIDI messages come in, then we will have a decidedly non-smooth step-wise output.

The naive approach is to directly filter incoming MIDI messages, however this ignores two factors
1) Time-related noise
2) Step-wise discontinuities due to the relatively slow rate of Midi messages

Every filtration step inevitably results in latency. It makes the most sense to filter the signal at the 
end of the process, at a regular rate.

----------------------------------------------------------------------------
NOTES ON FILTERS
----------------------------------------------------------------------------
Note the ripple present in the elliptic filter (above) near the start of the graph. 
	(FilterGainCalculator\plots\elliptic_filter__vs__raw_velocity.png)
This is likely a combination of the elliptic filter’s improved low-pass fall-off, since it occurs near a sawtooth, which naturally has lots of high frequencies. The waveform distortion introduced by the elliptic filter is likely phase related. We actually prefer the less aggressive performance of the Chebyshev filter in this situation. Both filters have overshoot, but the Chebyshev’s is less prominent.

----------------------------------------------------------------------------
Chebyshev Filter (2 Poles)
 	the slower falloff of 2 poles vs 4 poles means we need to shift it to a lower frequency to get better noise 	reduction. We shift it as far as possible to the left until without exceeding the desired latency of 24ms
	(four pole chebyshev) fc = 0.014 0 15% ripple 4 poles
----------------------------------------------------------------------------
Chebyshev Filter (4 Poles)
	Sharper peaks, which could be considered discontinuities, and thus not ideal
	In audio processing, we tend to dislike sudden changes like this, as discontinuities increase the odds of pops and crackles in the output audio

----------------------------------------------------------------------------
Alpha Beta Filter
	Note that the alpha beta function is a lot less adequate at dealing with noise. It is possible to chain several together to increase attenuation in the stop-band, but this was not as good as the Chebyshev. An advantage to this filter is the lack of overshoot near the start. 
----------------------------------------------------------------------------
Moving Average Filter
	The worst performing filter, as expected, since it is FIR.
----------------------------------------------------------------------------
Max/Min filter that tracks the local maxima and minima (ala maf), then averages them together,
and filters that figure through a chebyshev filter
	This was the preferred option, as implemented in MidiSmoother