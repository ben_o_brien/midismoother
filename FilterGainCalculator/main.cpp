/*------------------------------------------------------------------------------
 * The timing of MIDISmoother is non-deterministic due to the use of threads
 * designed to simulate receiving MIDI messages. These threads fire at uneven
 * rates, which make it hard to test the latency of smoothing algorithms.
 * This program allows us to test filtering in a more controlled setting
 * by running a non real-time loop representing the operation
 * of the real-time system. 

 * SUMMARY
 * midi_data_sz.csv was appended to midi_data_scratch_sz.csv to created
 * a new file called midi_data_sz_combined.csv, which can be found in
 * MidiSmoother/data subfolder of the overall project 
 *
 * Filters of increasing varying were calibrated to have approx. 25ms of latency
 * by comparing zero-crossings of a recorded (raw) output from the real-time application.
 * 
 * This works because the noise in the raw data is proportional to the signal strength,
 * giving relatively unobstructed zeros.
 *
 * Min/Max filters were tested for noise rejection potential, prior to the smoothin step.
 * The noise on the input waveforms exhibits a clear envelope, which can be estimated
 * using min and max filters of sufficient length. By smoothing the min and max values,
 * and then averaging them, we can get a fairly smooth and noise-free output signal.
 *
 * The following filters were tested for smoothing potential
 * Moving Average (FIR)
 * Alpha Beta (IIR) 
 * Chebyshev (IIR)
 * Elliptic (IIR)
 *
 * Alpha beta and moving average filters were insufficient at reducing noise in the required
 * time frame, but it's good design practice to try simple methods before building up complexity
 * if insufficient.
 *
 * The elliptic was the smoothest, but was prone to wobbles. due its stop-band ripple acting on
 * sharp edges in an undesirable manner
 * Ultimately the Chebyshev in a two pole rendition provided the best compromise between smoothness
 * and latency. 
 * It's slower falloff is actually preferred here, because harshly low-pass filtering sawtooth-like
 * or square-like waveforms can lead to unwanted ringing if the attenuation is too great 
 * (as with the elliptic filter)
 * To improve the Chebyshev configuration,
 * the signal was prefilted by averaging two simple min and max filters
 *
 * Created by Ben O'Brien on 18/02/22 
 *
 *///--------------------------------------------------------------------------


#include "filters.h"

#ifndef M_PI
#define M_PI 3.14159265359f
#endif

int main(int argc, const char * argv[])
{
    if(argc != 2)
    {
        std::cout << "Usage: " << argv[0] << " ";
        std::cout << "<[input].csv>" 
        <<'\n';
        
        return(-2);
    }
    
    const int block_size = 32;
    const double audio_sample_frequency = 44100.;
    const double mMidiValuesPerRevolution = 2048.;
    const double mSecondsPerRevolution = 1.8;
    
    // Approximately how often the velocity is sampled through VelocityConsumer 
    // by RequestMSToMoveValue
    // e.g. For a standard configuration of 32 sample block at 44100 hz this is 0.72ms
    double velocity_sample_period = static_cast<double>(block_size)/audio_sample_frequency;
    
    
    std::ifstream inputCSVFileStream;
    for (int i = 0; i < argc; ++i)
    {
        switch(i)
        {
            case 1:
                inputCSVFileStream = std::ifstream(argv[1]);
                if( !inputCSVFileStream )
                {
                    std::cout << "Failed to open file " << argv[1] << std::endl;
                    exit(-1);
                }
                break;
            default:
                break;
        }
    }
    
    
    std::vector<FilterZeroCrossingStats*> allStats;


    //---------------------------------------------------------------------
    // Create a moving average filter
    //---------------------------------------------------------------------
    // Create the moving average filter with length 60
    MovingAverageFilter MAFilter(60);
    
    FilterZeroCrossingStats movingAverageStats("Moving Average");
    allStats.push_back(&movingAverageStats);
    
    //---------------------------------------------------------------------
    // Create an alpha-beta filter
    //---------------------------------------------------------------------
    // slow attenuation in stop band at 0.0136, but conversely low latency
    // shift left (increasing latency) until it gets to 22ms delay
    RecursiveFilter alphaBetaLowPassFilter;    
    makeAlphaBetaFilter(0.00525, alphaBetaLowPassFilter); 
    
    FilterZeroCrossingStats alphaBetaStats("Alpha Beta");
    allStats.push_back(&alphaBetaStats);
    
    //---------------------------------------------------------------------
    // Create an alpha-beta cascasde
    //---------------------------------------------------------------------
    
    std::vector< std::unique_ptr<RecursiveFilter> > alphaBetaCascade;
    for(int i = 0; i < 3; i++)
    {
        alphaBetaCascade.emplace_back(std::make_unique<RecursiveFilter>());
        makeAlphaBetaFilter(0.0136, *alphaBetaCascade.back());
    }
    
    FilterZeroCrossingStats alphaBetaCascadeStats("Alpha Beta Cascade");
    allStats.push_back(&alphaBetaCascadeStats);
    
    
    //---------------------------------------------------------------------
    // Create a chebyshev filter (two pole variant)
    //---------------------------------------------------------------------
    RecursiveFilter chebyshevFilterTwoPole;
    
    makeChebyshevFilters(0.00775, RecursiveFilter::low_pass, 3, 2, chebyshevFilterTwoPole);
    FilterZeroCrossingStats chebyshevTwoPoleStats("Chebyshev 2 Pole");
    allStats.push_back(&chebyshevTwoPoleStats);
    
    //---------------------------------------------------------------------
    // Create a chebyshev filter (four pole variant)
    //---------------------------------------------------------------------
    RecursiveFilter chebyshevFilterFourPole;
    
    makeChebyshevFilters(0.014, RecursiveFilter::low_pass, 3, 4, chebyshevFilterFourPole);
    FilterZeroCrossingStats chebyshevFourPoleStats("Chebyshev 4 Pole");
    allStats.push_back(&chebyshevFourPoleStats);
    
    
    //---------------------------------------------------------------------
    // Create the Elliptic filter
    //---------------------------------------------------------------------
    // Consists of a series of simpler two pole filters, called biquads
    // each chained into the next. When we receive a new input value, we call 'double passthrough = input', 
    // then iterate through all filters calling 'passthrough = filter->update(passthrough)' 
    // the final value of passthrough is the output value
    std::vector< std::unique_ptr<RecursiveFilter> > ellipticFilterBiQuadCascade;
    
    makeEllipticFilterCascade(RecursiveFilter::low_pass, 0.0136*2.*M_PI, 0.5, 0.03*M_PI, -30, 
        ellipticFilterBiQuadCascade);
    FilterZeroCrossingStats ellipticFilterStats("Elliptic");
    allStats.push_back(&ellipticFilterStats);
    
    //---------------------------------------------------------------------
    // Min/Max filter
    //---------------------------------------------------------------------
    // N = 15 : ~9ms
    // N = 30 : ~12ms
    
    // Some points about the max and min filter. 
    // KEY: Some is good, more isn't always better. 
    
    // If the waveform is moving down
    //       max is a high latency indicator and will sample and hold peaks
    //       min is low latency indicator
    // If the waveform is moving up
    //       min is a high latency indicator and will sample and hold troughs
    //       max is a low latency indicator
    // The output result is very smooth when a chebyshev filter is applied to the mean of max and min,
    // but the longer the min/max filter, the lumpier it becomes, as peaks or troughs 
    // can exercise influence for longer after they have dissipated
    
    const unsigned int minMaxEnvelopeFilterLength = 12; 
    MaxFilter maxFilter(minMaxEnvelopeFilterLength);
    MinFilter minFilter(minMaxEnvelopeFilterLength);   


    RecursiveFilter minMaxSmoothingFilter;
    makeChebyshevFilters(0.0095, RecursiveFilter::low_pass, 1.5, 2, minMaxSmoothingFilter);
    FilterZeroCrossingStats minMaxMeanFilterStats("Chebyshev with Min Max Pre Filter");
    allStats.push_back(&minMaxMeanFilterStats);
    
    
    //---------------------------------------------------------------------
    // Statistics (zero crossing points) used to estimate latency for each method
    // Zero crossing points provide a basic approximation of latency that works for 
    // this particular sort of input data. We compare the crossing point of the real
    // data with the crossing point of the filtered data. This works
    // for this particular data set because the level of noise is roughly proportional 
    // to the speed, so there is almost no noise at the zero crossing.
    // If the data were extremely noisy around the crossing, the
    // efficacy of zero crossing based latency calculations would reduce.
    
    double lastInputZeroCrossing = -1.;
    
    
    
    //---------------------------------------------------------------------
    
    srand(static_cast<unsigned int>(time(0)));
    
    double t = 0.;//s 
    double period = 1.;
    
    // Approximately how often we get MIDI events
    
    // Vary this randomly according to normal distribution
    double average_midi_message_interval = 0.004;
    // s  - acutally a normal distribution between 0.002 to 0.009, usually around 0.003 to 0.005
    double midi_message_interval = average_midi_message_interval;
    
    int numSamples = static_cast<int>(period/velocity_sample_period)+1;
    
    const double sine_frequency = 2.;//hz
    const double noiseMagnitude = 4.;
    const double signalMagnitude = 30.;
    
    double lastGroundTruth = 0.;
    double lastVelocityValue = 0.;
    double midiTriggerTimer = 0.;

	int midi_value;
	char comma;
	std::string tab_delim_line;
    
    int i = 0;
    

             
    

    

    bool atLeastOneMIDIEventFound = false;

    // MIDI Event count
    // Smoothing behaviour is based on whether we have received a MIDI value
    // We cannot assume velocity is zero just because we haven't yet received a
    // MIDI value. Generally we only need to count the first few, so in practice
    // we can avoid incrementing this count past 2 events
    int midiEventCount = 0;       
    
    //---------------------------------------------------------------------
    // Output files  
    std::string latencyFileName = "latency.txt";
    std::ofstream latencyOutputFile(latencyFileName, std::ofstream::out); 
    
    std::ostringstream outputFileName;
    outputFileName << "output.csv";
    std::ofstream outputFile(outputFileName.str(), std::ofstream::out); 

    //---------------------------------------------------------------------
    // Printing header for output.csv
    outputFile << "Time";
    outputFile << ",Raw Velocity";
    for(FilterZeroCrossingStats* stat : allStats)
    {
        outputFile << ',' << stat->tag;
    }
    outputFile << ',' << "Max";
    outputFile << ',' << "Min";
    outputFile << "\n";
    
    
    //---------------------------------------------------------------------
    // Read and parse first line of input csv
    std::getline( inputCSVFileStream, tab_delim_line );
    std::istringstream string_stream( tab_delim_line );
    string_stream >> midi_message_interval;
    string_stream >> comma;
    string_stream >> midi_value;
    
    //---------------------------------------------------------------------
    // Simulating the operation of MIDISmoother.exe, but not real-time
    // We want purely deterministic timing in order to properly measure
    // latency, and to compare multiple filters in a simultaneous fashion
	while( !inputCSVFileStream.eof() && midi_message_interval >= 0)
	{
        if(midiTriggerTimer > midi_message_interval)
        {
            double messageFrequency = 1./(static_cast<float>(midi_message_interval));

            double mWheelMovementSinceLastMessage = midi_value/static_cast<double>(mMidiValuesPerRevolution);
            
            double oldVelocityValue = lastVelocityValue;
            lastVelocityValue = mWheelMovementSinceLastMessage * mSecondsPerRevolution * messageFrequency;
            
            if(isZeroCrossing(oldVelocityValue, lastVelocityValue))
            {
                // Zero crossing point
                latencyOutputFile << "Zero Crossing (Raw): " << t << '\n';
                lastInputZeroCrossing = t;
            }
            
            midiTriggerTimer -= midi_message_interval;

            // Read and parse new line
            std::getline( inputCSVFileStream, tab_delim_line );
            std::istringstream string_stream( tab_delim_line );
            string_stream >> midi_message_interval;
            string_stream >> comma;
            string_stream >> midi_value;
            
            if(midiEventCount < 2)
                midiEventCount ++;
            
        }
        
        if(midiEventCount > 0)
        {


            //--------------------------------------------------
            // Moving average filter (FIR)
            //--------------------------------------------------
            double MAFilterOutput = MAFilter.update(lastVelocityValue);
            movingAverageStats.update(t, MAFilterOutput, latencyOutputFile, lastInputZeroCrossing, midiEventCount);
            
            //----------------------------------------------------------------------------------------------------
            // Alpha-Beta filter (IIR)
            //----------------------------------------------------------------------------------------------------
            double alphaBetaFilterOutput = alphaBetaLowPassFilter.update(lastVelocityValue);
            alphaBetaStats.update(t, alphaBetaFilterOutput, latencyOutputFile, lastInputZeroCrossing, midiEventCount);
            
            //----------------------------------------------------------------------------------------------------
            // Alpha-Beta Cascade filter (IIR)
            //----------------------------------------------------------------------------------------------------
            double alphaBetaCascadeFilterOutput = lastVelocityValue;
            for(auto& cascadeElement : alphaBetaCascade)
            {
                alphaBetaCascadeFilterOutput = cascadeElement->update(alphaBetaCascadeFilterOutput);
            }
            
            alphaBetaCascadeStats.update(t, alphaBetaCascadeFilterOutput, latencyOutputFile, 
                                            lastInputZeroCrossing, midiEventCount); 
            
            //----------------------------------------------------------------------------------------------------
            // Chebyshev Two Pole
            //----------------------------------------------------------------------------------------------------
            double chebyshevFilterTwoPoleOutput = chebyshevFilterTwoPole.update(lastVelocityValue);
            chebyshevTwoPoleStats.update(t, chebyshevFilterTwoPoleOutput, latencyOutputFile, 
                                            lastInputZeroCrossing, midiEventCount);

            //----------------------------------------------------------------------------------------------------
            // Chebyshev Four Pole
            //----------------------------------------------------------------------------------------------------
            double chebyshevFilterFourPoleOutput = chebyshevFilterFourPole.update(lastVelocityValue);
            chebyshevFourPoleStats.update(t, chebyshevFilterFourPoleOutput, latencyOutputFile, 
                                            lastInputZeroCrossing, midiEventCount);


            //----------------------------------------------------------------------------------------------------
            // Elliptic
            //----------------------------------------------------------------------------------------------------
            // Elliptic filter uses a chain of biquads
            double ellipticFilterOutput = lastVelocityValue;
            for(auto& twoPoleFilter : ellipticFilterBiQuadCascade)
            {
                ellipticFilterOutput = twoPoleFilter->update(ellipticFilterOutput);
            }
            ellipticFilterStats.update(t, ellipticFilterOutput, latencyOutputFile, lastInputZeroCrossing, midiEventCount);

            //--------------------------------------------------
            // Min/Max filter
            //--------------------------------------------------
            double maxFilterValue = maxFilter.update(lastVelocityValue);
            double minFilterValue = minFilter.update(lastVelocityValue);
            
            // Elliptic filter uses a chain of biquads
            // Assume symmetrical noise
            double minMaxMeanFilterOutput = minMaxSmoothingFilter.update((maxFilterValue + minFilterValue)*0.5);
            minMaxMeanFilterStats.update(t, minMaxMeanFilterOutput, 
                                        latencyOutputFile, lastInputZeroCrossing, midiEventCount);
            
            
            //----------------------------------------------------------------------------------------------------
            // Printing output to file
            //----------------------------------------------------------------------------------------------------
            outputFile  << t << ',' << lastVelocityValue;
            
            for(FilterZeroCrossingStats* stat : allStats)
            {
                outputFile << ',' << stat->previousFilterOutput;
            }
           
            //Printing max and min values
            outputFile << ',' << maxFilterValue;
            outputFile << ',' << minFilterValue;
           
            
            outputFile << '\n';
        } else
        {
            outputFile << t << ','                  //time
                    << lastVelocityValue;           //lastVelocity 
                    
                    for(FilterZeroCrossingStats* stat : allStats)
                    {
                        outputFile << ',' << 0;
                    }
                  
            outputFile << ',' << 0                  //max
                    << ',' << 0 << '\n';            //min
        }



        t += velocity_sample_period;
        
        //-----------------------------------------
        midiTriggerTimer += velocity_sample_period;
    }
    
    for(FilterZeroCrossingStats* stat : allStats)
    {
        if(!stat->latencyReadings.empty())
        {
            double averageLatencyFromZeroCrossings = 0.;
            for(double l : stat->latencyReadings)
            {
                averageLatencyFromZeroCrossings += l;
            }
            
            averageLatencyFromZeroCrossings /= stat->latencyReadings.size();
            std::cout << "Average Latency (";
            std::cout << stat->tag << "): " << averageLatencyFromZeroCrossings*1000. << "ms" << '\n';
        } else
        {
            outputFile << ' ';
        }
    }
    
    
    
    std::cout << "Wrote results to " << outputFileName.str() << " in the application directory" << '\n';
    std::cout << "Wrote latency zero crossings to " << latencyFileName << " in the application directory" << '\n';
    
    
    outputFile.close();

  
    
    return(0);
}
