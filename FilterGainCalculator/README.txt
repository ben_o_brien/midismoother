Usage: FilterGainCalculator.exe "..\..\data\midi_data_sz_combined.csv"

This program simulates the operation of MidiSmoother while testing the operation
of multiple different types of filters. It runs these against raw velocity data and
estimates the latency of each type by comparing zero crossings.
 * Useable latency measurements require input data with many smooth zero crossings
 * For example, midi_data_sz_scratch.csv is suitable, but midi_data_sz.csv is not,
 & as the latter is a ramp down with next to no zero crossings
 * Recommend using midi_data_sz_combined.csv for this reason

It outputs the filtering results in output.csv.

For example, here is a small portion of the output in output.csv (tabs added for readability)
.........................................................................................................
Time,		Raw Velocity,	Moving Average,	Alpha Beta,	Alpha Beta Cascade,	...	
0,		0,		0,		0,		0,		0,	...	
0.000725624,	0,		0,		0,		0,		0,	...
.........................................................................................................

Can be built using either
	a) CMakeLists 
	b) The provided visual studio solution

 * This application uses filters.cpp and filters.h
 * If generating makefiles using cmake, make sure to specify PATH_TO_FILTERS_H as the source directory of the  
 * MIDISmoother project after calling configure. PATH_TO_FILTERS_H is the directory where filters.h is found

 * The timing of MIDISmoother is non-deterministic due to the use of threads
 * designed to simulate receiving MIDI messages. These threads fire at uneven
 * rates, which make it hard to test the latency of smoothing algorithms.
 * This program allows us to test filtering in a more controlled setting
 * by running a non real-time loop representing the operation
 * of the real-time system. 

 * SUMMARY
 * midi_data_sz.csv was appended to midi_data_scratch_sz.csv to created
 * a new file called midi_data_sz_combined.csv, which can be found in
 * MidiSmoother/data subfolder of the overall project 
 *
 * Filters of increasing varying were calibrated to have approx. 25ms of latency
 * by comparing zero-crossings of a recorded (raw) output from the real-time application.
 * 
 * This works because the noise in the raw data is proportional to the signal strength,
 * giving relatively unobstructed zeros.
 *
 * Min/Max filters were tested for noise rejection potential, prior to the smoothing step.
 * The noise on the input waveforms exhibits a clear envelope, which can be estimated
 * using min and max filters of sufficient length. By smoothing the min and max values,
 * and then averaging them, we can get a fairly smooth and noise-free output signal.
 *
 * The following filters were tested for smoothing potential
 * Moving Average (FIR)
 * Alpha Beta (IIR) 
 * Chebyshev (IIR)
 * Elliptic (IIR)
 *
 * Alpha beta and moving average filters were insufficient at reducing noise in the required
 * time frame, but it's good design practice to try simple methods before building up complexity
 * if insufficient.
 *
 * The elliptic was the smoothest, but was prone to wobbles. due its stop-band ripple acting on
 * sharp edges in an undesirable manner
 * Ultimately the Chebyshev in a two pole rendition provided the best compromise between smoothness
 * and latency. 
 * It's slower falloff is actually preferred here, because harshly low-pass filtering sawtooth-like
 * or square-like waveforms can lead to unwanted ringing if the attenuation is too great 
 * (as with the elliptic filter)
 * To improve the Chebyshev configuration,
 * the signal was pre-filtered by averaging two simple min and max filters


