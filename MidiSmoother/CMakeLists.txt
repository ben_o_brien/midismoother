cmake_minimum_required(VERSION 3.10.2)

project(MidiSmoother VERSION 1.0)

add_executable(MidiSmoother main.cpp MidiSmoother.cpp MidiSmoother.h filters.cpp filters.h Input/MidiFirer.cpp Input/MidiFirer.h Output/SineWaveRecorder.cpp Output/SineWaveRecorder.h Output/VelocityConsumer.cpp Output/VelocityConsumer.h)

target_include_directories(MidiSmoother
    PUBLIC
        ${PROJECT_SOURCE_DIR} ${PATH_TO_FILTERS_H}
)

set_target_properties(MidiSmoother PROPERTIES LINKER_LANGUAGE CXX)