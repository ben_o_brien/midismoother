//-----------------------------------------------------------------------------
//
//  main.cpp
//  MidiFirer
//
//  Created by Nathan Holmberg on 8/04/14.
//
//  Edited by Ben O'Brien on 18/02/22 
//
//-----------------------------------------------------------------------------


#include <iostream>
#include <fstream>

#include "Input/MidiFirer.h"
#include "Output/VelocityConsumer.h"


void PrintUsage( const char* binary_name )
/*
 * Prints the usage for this application
 *
 * @param binary_name
 *		The name of the current binary
 */
{
	std::cout << "Usage: " << binary_name << " <midi_file>" << std::endl;
	exit(-1);
}


int main(int argc, const char * argv[])
{
	if( argc < 2 )
	{
		PrintUsage( argv[0]);
	}

	std::string output = (argc >= 3) ? argv[2] : "output.wav";

	// The values for the smoother are from the real world. This particular device has 2048 'clicks' around it's wheel
	// and all devices have one revolution is 1.8 seconds (it's a DJ thing)
    
    // FACTORS REDUCING SMOOTHNESS OF OUTPUT VELOCITY
    // 1) Noise from digital rotary encoders
    //      The greater the motion of the rotary encoder, the less accurate it becomes due to proportional noise
    // 2) 'Noise' from timing
    //      The signal is distorted further by the need to divide it by incoming time delta values, which 
    //      are variable (estimated ~3.5 with a std deviation of 1.)
    //      Incoming MIDI status messages give us only the change in position, not the speed
    //      To calculate the speed, we need to divide the output by the time between each event,
    //      and since we need to measure this time, we are prone to timing-based noise
    // 3) Stepwise Motion
    //      In the sample data, we recieve MIDI events approximately every 2-5ms
    //      The smoother is polled by VelocityConsumer at 0.72ms (720 microseconds
    //      This means that the velocity consumer usually polls at approximately 3-7 times higher than velocity
    //      is updated. This creates stepwise motion if not smoothed

    const double sampleRate = 44100.;
    const double samplesBerBlock = 32.;
    
	MidiSmoother smoother( 2048, 1.8, samplesBerBlock, sampleRate);
    MidiFirer firer( smoother );
    VelocityConsumer consumer( smoother, output );
	
	// Load MIDI data from the supplied file argument
	{
		std::ifstream filestream = std::ifstream(argv[1]);
		if( !filestream )
		{
			std::cout << "Failed to open file " << argv[1] << std::endl;
			exit(-1);
		}
	
		firer.LoadMidiDataFromStream(filestream);
	}
    
	// Start the firer and consumer
    firer.Start();
	consumer.Start();
    firer.WaitForCompletion();
    consumer.WaitForCompletion();
    smoother.WaitForCompletion();
    
    return 0;
}

