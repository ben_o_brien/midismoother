//-----------------------------------------------------------------------------
//
//  MidiSmoother.cpp
//  MidiSmoother
//
//  Created by Nathan Holmberg on 9/04/14.
//
//  Edited by Ben O'Brien on 18/02/22
//
//-----------------------------------------------------------------------------

#include "MidiSmoother.h"
#include <iostream>
#include <cmath>

#define MICROSECONDS_PER_SECOND 1000000.f
#define MILLISECONDS_PER_SECOND 1000.f
#define SECONDS_PER_MICROSECOND 0.000001f

#ifndef M_PI
#define M_PI 3.14159265359f
#endif

// If set to 0, filtering is done upon call of RequestMSToMoveValue
// If set to 1, a separate thread will be initialised, which will handle
// smoothing separately at a rate of mSmoothingFilterThreadUpdateInterval_us
#define USE_FILTERING_THREAD 1

// If set to 0, only a Chebyshev filter is used.
// If set to 1, a minimum and maximum filter are averaged together,
// then fed into a slightly less aggressive Chebyshev filter
#define USE_MIN_MAX_FILTER_PRE_SCREEN 1


//-----------------------------------------------------------------------------
MidiSmoother::MidiSmoother( int midi_values_per_revolution, double seconds_per_revolution, 
            double audio_samples_per_block, double audio_sample_rate) :
                    mMidiValuesPerRevolution( midi_values_per_revolution ),
                    mSecondsPerRevolution( seconds_per_revolution ),
                    mbMidiIsProcessing(false),
                    mLastVelocityRaw(0.),
                    mLastVelocityFiltered(0.),
                    mHasNotReceivedAnyMidiMessages(true),
                    mLastNonZeroIntervalCount(-1),
                    mFilteringThread()
/*
 * Constructor for a Midi Smoother.
 *
 * @param midi_values_per_revolution
 *		The number of midi values that would need to be recieved for an entire platter revolution to be expected
 * @param seconds_per_revolution
 *		The number of seconds an entire platter revolution represents
 * @param audio_samples_per_block
 *      Usually 32 per block
 *      The block size handled by each call to RequestMSToMoveValue.
 *      Required in order to set the speed of the smoothing thread (if used) and parameters of the filters
 * @param audio_sample_rate
 *		The audio sampling rate (e.g. 44100.)
 *      Required in order to set the speed of the smoothing thread (if used) and parameters of the filters
 */
{

    
    // If USE_FILTERING_THREAD == 0, Filter update is tied to calls of RequestMSToMoveValue 
    double filterUpdateRate = audio_sample_rate / audio_samples_per_block; //Hz
    
    
    mSmoothingFilterThreadUpdateInterval_us = MICROSECONDS_PER_SECOND / filterUpdateRate;
    
    //std::cout << "mSmoothingFilterThreadUpdateInterval_us = " << mSmoothingFilterThreadUpdateInterval_us << '\n';
    
    // Filters were designed for a 44100.0/32.0 update rate,
    // so just in case we want to change the thread rate later,
    // we include a frequencyRatio which will impact the gains
    // of the filters we use. This is experimental, and has 
    // not had extensive testing 
    double frequencyRatio = filterUpdateRate/1378.125;
    
    #if USE_MIN_MAX_FILTER_PRE_SCREEN
        // For a block size of 32., and a sample rate of 44100., ths works out to
        // 12
        int minMaxFilterLength = static_cast<int>(12.*frequencyRatio);
        if(minMaxFilterLength <= 0)
        {
            minMaxFilterLength = 3;
        }
        mMaxFilter.setOrder(minMaxFilterLength);
        mMinFilter.setOrder(minMaxFilterLength);
        
        // The ideal cutoff frequency is approx 0.0136, however a two pole chebyshev filter
        // does not provide enough stop band attenuation to make this work, and a four pole
        // rendition distorts the passband too much, introducing an unacceptable level of ripple.
        // However, the two pole has the advantage of low latency, so we can afford to move
        // the cutoff frequency to the left (taking advantage of the weaker falloff to retain
        // frequencies in the transition region). In this case, we choose values around f_c/2
        
        // Pre-filtering introduces latency, so the cutoff frequency is set to 0.009
        makeChebyshevFilters(0.009*frequencyRatio, RecursiveFilter::low_pass, 2, 2, mLowPassChebyshevFilter);
    #else
        // The lack of prefiltering here means that we can afford to sacrifice some latency 
        // and push the Chebyshev filter cutoff lower      
        makeChebyshevFilters(0.0075*frequencyRatio, RecursiveFilter::low_pass, 2, 2, mLowPassChebyshevFilter);
    #endif
}

MidiSmoother::~MidiSmoother()
/*
 * Destructor for the MidiSmoother
 */
{
    StopMidiProcessing();
}


void MidiSmoother::FilteringThreadFunction()
/* The filtering thread (only used if USE_FILTERING_THREAD is set to 1, 
 * otherwise filtering is completed within RequestMSToMoveValue)
 * 
 * Velocity is low pass filtered to remove noise from two locations
 * 1) Noise from the MIDI values
 * 2) Noise from the uneven and asynchronous intervals between them
 * It is also filtered to deal with the decidely unsmooth step-wise motion created by
 * the velocity polling rate being 3-7 times higher than incoming midi status
 * event update it.
 * 
 * The low-pass filter updates at a regular interval to bypass step-wise behaviour,
 * which occurs when the velocity is polled faster than the speed at which MIDI events occur.
 * While it would be possible to smooth within the RequestMSToMoveValue
 * by removing the const modifier, it wouldn't be ideal, since it
 * would require refactoring of external classes (which aren't meant to be modified)
 
 * Filtering starts upon receipt of first event
 */
{
    double microSecondsInterval = std::floor(mSmoothingFilterThreadUpdateInterval_us); 
    std::chrono::microseconds sleep_interval = std::chrono::microseconds(static_cast<long long>(microSecondsInterval)) ;
    
    // Predeclaring variables
    double newVelocity;
    long long microseconds;
    std::chrono::microseconds interval;
    std::chrono::duration<int,std::micro> duration_micro;
    std::chrono::steady_clock::time_point end;
    while(isFilteringThreadRunning)
    {
		std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        
        if(mHasNotReceivedAnyMidiMessages)
        {
            // Filter has not yet been activated
            mLastVelocityFiltered = 0.;
        } else
        {
            newVelocity = 
                mLowPassChebyshevFilter.update
                (
                    0.5*(mMaxFilter.update(mLastVelocityRaw) + mMinFilter.update(mLastVelocityRaw))
                );
            mLastVelocityFiltered = newVelocity;
        }
        
		end = std::chrono::steady_clock::now();
		duration_micro = std::chrono::duration_cast< std::chrono::duration<int,std::micro> >(sleep_interval - (end-start));
        microseconds = duration_micro.count();
        interval = std::chrono::microseconds( microseconds ) ;
        
        // Waiting for next processing step
        if(interval.count() > 0)
        {
            std::this_thread::sleep_for(interval);
        }
    }
}


void MidiSmoother::StartFilteringThread()
/*
 * Starts the filtering/smoothing thread
 * A private function that is only called if the 
 * thread-based implementation of smoothing is specified
 */
{
    mFilteringThread = std::thread( &MidiSmoother::FilteringThreadFunction, this );
    isFilteringThreadRunning = true;
}


void MidiSmoother::StopFilteringThread()
/*
 * Halts the filtering/smoothing thread
 * A private function that is only called if the 
 * thread-based implementation of smoothing is specified
 */
{   
    {
        isFilteringThreadRunning = false;
    }
    
    if( mFilteringThread.joinable())
    {
        mFilteringThread.join();
    }
}


void MidiSmoother::StartMidiProcessing()
/*
 * Indicates that the midi processing is about to begin!
 */
{
    mTimeOfLastNotifyMidiValue = std::chrono::steady_clock::now();;
    mbMidiIsProcessing = true;
    
#if USE_FILTERING_THREAD == 1
    StartFilteringThread();
#endif
}

void MidiSmoother::StopMidiProcessing()
/*
 * Indicates that there is no more midi coming!
 */
{
	mbMidiIsProcessing = false;
    
#if USE_FILTERING_THREAD == 1
    StopFilteringThread();
#endif
}

bool MidiSmoother::MidiIsProcessing() const
/*
 * Indicates if the smoother is currently active and processing MIDI
 * 
 * @return
 *		If midi is currently processing
 */
{
	return mbMidiIsProcessing;
}


void MidiSmoother::WaitForCompletion()
/*
 * Waits for the thread to end
 */
{
#if USE_FILTERING_THREAD == 1
	if( mFilteringThread.joinable() )
		mFilteringThread.join();
#endif
}


void MidiSmoother::NotifyMidiValue( char midi_value )
/*
 * Notify the smoother that a new value has been received from the platter.
 *
 * @param midi_value
 *		The number of midi values that has passed. This can be negative indicating reverse direction.
 */
{
    // Use steady clock for accuracy
    auto start = std::chrono::steady_clock::now();
    auto interval = std::chrono::duration_cast<std::chrono::microseconds>(start - mTimeOfLastNotifyMidiValue);
    mTimeOfLastNotifyMidiValue = start;
    
    long long int intervalCount = interval.count();   
    if(intervalCount > 0)
    {
        mLastNonZeroIntervalCount = intervalCount;
    }     

    // Note: We count the interval in microseconds rather than milliseconds to give ourselves 3sf more precision
    // However, on observation, MIDI Events can usually arrive in multiples of this number
   
    // 1000 hz was the initial value for the time factor, representing a consistent period of 1ms between each MIDI message 
    // However, in the sample data, and in practice,
    // the period between events is variable, tending to span from 2 to 5 ms, sometimes longer.
    // When the first MIDI event arrives, we have essentially no idea what velocity it represents,
    // since the delta is completely unknown. In this case, we simply default to a message frequency of 333.f,
    // which is close to the average message speed in the sample data. This shouldn't be especially problematic
    // when we consider that it only applies to the first measurement, and thus the inaccuracy will
    // iron itself out very quickly
    
    double messageFrequency; 
    if(mLastNonZeroIntervalCount > 0)
    {
        messageFrequency = MICROSECONDS_PER_SECOND/static_cast<double>(mLastNonZeroIntervalCount); 
        // e.g. if we received one event every millisecond, messageFrequency would be 1000,000/1000 = 1000Hz
    } else if(mLastNonZeroIntervalCount == 0)
    {
        // This is an edge case, and would require two events to occur at exactly the same time
        // (a divide by zero case) so default to a 1000hz frequency
        // We can't rule out this edge case from occurring
        messageFrequency = 1000.; 
    } else// mLastNonZeroIntervalCount has not been set (still initialised to -1) 
    {
        messageFrequency = 1000.;
    }
    
    //Updating the velocity    
    double mWheelMovementSinceLastMessage = midi_value/static_cast<double>(mMidiValuesPerRevolution);
    
    double newVelocity = mWheelMovementSinceLastMessage * mSecondsPerRevolution * messageFrequency;
    
    // Trying to keep it atomic, since mLastVelocityRaw will be accessed elsewhere
    
    // Why not filter at all in notifyMIDIStatus?
    // 1) notifyMIDIstatus is called at irregular time intervals, 
    //    and thus can be considered to have an irregular sampling frequency. 
    //    This means that the filter coefficients would need to be constantly re-calculated
    //    Longer gaps would necessarily change the cutoff frequency requirements
    //
    // 2) Blocks of audio are read quicker (approx 4 times faster) than notifyMIDIStatus is called. 
    //    This leads to a stepwise output motion
    //
    // Thus the best result is to updating the smoothing filter system at a regular rate
    // Since RequestMSToMoveValue is called every time a block is accessed, we piggyback off it
    mLastVelocityRaw = newVelocity;


    if(mHasNotReceivedAnyMidiMessages)
    {
        mHasNotReceivedAnyMidiMessages = false;
    }
    

}


double MidiSmoother::RequestMSToMoveValue( double ms_to_process )
/*
 * Request a ms to move value from the smoother. This is so the audio engine would know how much to step forward to produce the appropriate pitch for what the platter is doing.
 *
 * @param ms_to_process
 *		The number of ms we are calculating this step for. 
 * @return
 *		The number of ms that should be moved during this process step
 */
{   
#if USE_FILTERING_THREAD == 0 
    if(mHasNotReceivedAnyMidiMessages)
    {
        // Filter has not yet been activated
        mLastVelocityFiltered = 0.;
    } else
    {
        double rawVel = mLastVelocityRaw;
        mLastVelocityFiltered = 
            mLowPassChebyshevFilter.update
            (
                0.5*(mMaxFilter.update(rawVel) + mMinFilter.update(rawVel))
            );
    }
    return(mLastVelocityFiltered * ms_to_process);
    
#else // Otherwise, filtering is handled by a separate thread
    double returnVal = mLastVelocityFiltered;
    return(returnVal * ms_to_process);
#endif
}