//-----------------------------------------------------------------------------
//
//  Filters.cpp
//  MidiSmoother
//
//  Created by Ben O'Brien on 18/02/22
//
//  Except for the following specifically labelled functions
//      getChebyshevBiQuads 
//            (adapted from visual basic code by Steven W Smith - http://www.dspguide.com/ch20/2.htm)
//      makeChebyshevFilters 
//            (adapted from visual basic code by Steven W Smith - http://www.dspguide.com/ch20/2.htm)
//      bilinearTransform 
//            (https://codeocean.com/capsule/6580907/tree/v1)
//      makeEllipticFilterCascade 
//            (https://codeocean.com/capsule/6580907/tree/v1)     
//
//-----------------------------------------------------------------------------
#include "filters.h"
#include <iostream>
#include <array>
//-----------------------------------------------------------------------------
#ifndef M_PI
#define M_PI 3.14159265359f
#endif

#define FILTER_FILE_VERBOSE 0

//-----------------------------------------------------------------------------
// Utility functions

bool isOdd(int numberToCheck)
/*
 * Returns true if input is an odd number, to make code more readable.
 *
 * @param numberToCheck
 *		The number we want to check
 *
 * @return
 *		true if input is odd, false if even
 */
{
    return(numberToCheck % 2);
}


bool isEven(int numberToCheck)
/*
 * Returns true if input is an even number, to make code more readable.
 *
 * @param numberToCheck
 *		The number we want to check
 *
 * @return
 *		true if input is even, false if odd
 */
{
    return(numberToCheck % 2 == 0);
}


//-----------------------------------------------------------------------------

void bilinearTransform(RecursiveFilter::filterType filter_type, double a, double b, double c,
    double& a_output, double& b_output, double& c_output)
/*
 * From https://codeocean.com/capsule/6580907/tree/v1
 * (BiquadFilter::BilinearTransformation)
 * Applies the bilinear transformation to a polynomial in s space with form
 * Numerator: s^2 + a  Denominator: s^2 + b*s + c
 *  i.e. (s^2 + a)/(s^2 + b*s + c)
 * and returns the resultant gains in z space
 * 
 * @param filter_type
 *		Accepts RecursiveFilter::low_pass or RecursiveFilter::high_pass
 *
 * @param a
 *		input gain a 
 *
 * @param b
 *		input gain b 
 *
 * @param c
 *		input gain c
 *
 * @param a_output
 *		output gain a
 *
 * @param b_output
 *		output gain b
 *
 * @param c_output
 *		output gain c
 */
{
    a_output = 4.*a + 2.*b + c;
    c_output = 4.*a - 2.*b + c;
    
    switch(filter_type)
    {
    case RecursiveFilter::low_pass:
        b_output = -8.*a + 2.*c;
        break;
    case RecursiveFilter::high_pass:
        b_output = 8.*a - 2.*c;
    }
}


//-----------------------------------------------------------------------------
bool makeEllipticFilterCascade(RecursiveFilter::filterType filter_type, 
    double cutFreq_rads, 
    double passBand_dB, 
    double stopFreq_rads, 
    double stopBand_dB, 
    std::vector< std::unique_ptr<RecursiveFilter> >& biquadFilterCascade)
// 
/* 
 * Function derived from https://codeocean.com/capsule/6580907/tree/v1
 * Creates a cascade of two pole elliptic filters that can be chained in series
 * to approximated higher order versions.
 *
 * @param filter_type 
 *      Either RecursiveFilter::low_pass or RecursiveFilter::high_pass
 * 
 * @param cutFreq_rads      
 *      Expects inputs between 0. to 2PI
 *      The point at which we start transitioning to the stop band
 *      Frequency cutoff is relative to the sampling frequency, so make sure to account for this
 *      before passing it in
 *
 * @param passBand_dB
 *      The level of ripple allowed in the pass band in decibels
 *
 * @param stopFreq_rads      
 *      Expects inputs between 0. to 2PI
 *      The stop frequency is the point at which we enter the stop band
 *      Relative to the sampling frequency, so make sure to account for this
 *      before passing it in
 *
 * @param biquadFilterCascade 
 *      Container for the output filter as a two pole cascade
 */
{
	const double Ap = fabs(passBand_dB);
	const double As = fabs(stopBand_dB);
	const double wp = 2.0*tan(cutFreq_rads/2.0);
	const double ws = 2.0*tan(stopFreq_rads/2.0);
	const double k = wp/ws;
	const double kk = sqrt(sqrt(1.0-k*k));
    
    // EQ. (5.2)
	const double u = (1.0 - kk)/(2.0*(1.0 + kk));  
	const double u4 = u*u*u*u;
	const double u5 = u4*u;
	const double u9 = u4*u5;
	const double u13 = u4*u9;
    
    // EQ. (5.1)
	const double q = u + 2.0*u5 + 15.0*u9 + 150.0*u13; 
    
    // EQ. (5.3)
	const double D = (pow(10.0, As/10.0) -1.0)/(pow(10.0, Ap/10.0) -1.0); 
	const double dn = (log10(16.0*D))/(log10(1.0/q));
    
    // EQ. (5.4)
	const int in = (int)ceil(dn);
	const int n = (in %2 == 0) ? in : in +1; // Force even
    
    // EQ. (5.5)
	const double actual_As = 10.0*log10(1.0 + (pow(10.0, Ap/10.0)-1.0)/(16.0*pow(q, (double)n))); 
	
    // EQ. (5.8)
    const double alpha = sqrt(wp*ws); 
	const double vv = pow(10.0, Ap/20.0);
    
    // EQ. (5.12)
	const double V = (1.0/(2.0*n))*log((vv + 1.0)/(vv - 1.0)); 
    
    
	// EQ. (5.13)
	double pp;
	int m;
	// Numerator
	double sum_numerator = 0.0;
	for (m = 0; m < 100; m++)
	{
		pp = pow(q, m*(m+1))*sinh((2*m+1)*V);
		if (m%2 != 0) pp = -pp;
		sum_numerator += pp;
		if (fabs(pp)/fabs(sum_numerator) < 1.0e-10*fabs(sum_numerator)) 
            break; // done
	}
	// Denominator
	double sum_denominator = 0.5;
	for (m = 1; m < 100; m++)
	{
		pp = pow(q, m*m)*cosh(2*m*V);
		if (m%2 != 0) pp = -pp;
		sum_denominator += pp;
		if (fabs(pp)/fabs(sum_denominator) < 1.0e-10*fabs(sum_denominator)) 
            break; // done
	}
	const double p0 = fabs(sqrt(sqrt(q))*sum_numerator/sum_denominator);
    
    // EQ. (5.14)
	const double W = sqrt((1.0 + (p0*p0)/k)  *  (1.0 + k*p0*p0)); 
	const bool is_n_even = (n%2 == 0);
    
	const int r = (is_n_even ? n/2 : (n-1)/2);
    
	// EQ. (5.15)
    
    double* X = new double[r];

	int i;
	double mu;
	for (i = 1; i <= r; i++)
	{
		mu = is_n_even ? (i-0.5) : (double)i;
		// Numerator
		sum_numerator = 0.0;
		for (m = 0; m < 100; m++)
		{
			pp = pow(q, m*(m+1))*sin((2*m+1)*mu*M_PI/n);
			if (m%2 != 0) pp = -pp;
			sum_numerator += pp;
			if (fabs(pp)/fabs(sum_numerator) < 1.0e-10*fabs(sum_numerator)) 
                break; // done
		}
		// Denominator
		sum_denominator = 0.0;
		for (m = 1; m < 100; m++)
		{
			pp = pow(q, m*m)*cos(2.0*m*mu*M_PI/n);
			if (m%2 != 0) pp = -pp;
			sum_denominator += pp;
			if (fabs(pp)/fabs(sum_denominator) < 1.0e-10*fabs(sum_denominator)) 
                break; // done
		}
		X[i-1] = (2.0*sqrt(sqrt(q))*sum_numerator) / (1.0 + 2.0*sum_denominator);
	}
	// EQ. (5.16)

    double* Y = new double[r];
	for (i = 1; i <= r; i++)
	{
		Y[i-1] = sqrt((1.0 - (X[i-1]*X[i-1]/k))  *  (1.0 - k*X[i-1]*X[i-1]));
	}
	// EQ. (5.17)
    double* a_ = new double[r];
	for (i = 1; i <= r; i++)
	{
		a_[i-1] = 1.0/(X[i-1]*X[i-1]);
	}
	// EQ. (5.18)
	double* b_ = new double[r];
	for (i = 1; i <= r; i++)
	{
		b_[i-1] = 2.0*p0*Y[i-1]/(1.0 + p0*p0*X[i-1]*X[i-1]);
	}
	// EQ. (5.19)
	double* c_ = new double[r];
	double tmp;
	for (i = 1; i <= r; i++)
	{
		pp = p0*Y[i-1];
		tmp = X[i-1]*W;
		c_[i-1] = pp*pp + tmp*tmp;
		//
		tmp = 1.0 + p0*p0*X[i-1]*X[i-1];
		c_[i-1] /= (tmp*tmp);
	}
	// EQ. (5.20)
	tmp = 1.0;
	for (i = 1; i <= r; i++)
        tmp *= (c_[i-1]/a_[i-1]);
	const double H0 = (is_n_even == true) ? (pow(10.0, -Ap/20.0)*tmp) : (p0*tmp);
    
	// EQ. (5.22)
	const double k0 = H0; // (is_n_even == true) ? H0 : ( (H0*alpha)/(s + alpha*p0));
	for (i = 0; i < r; i++)
	{
		a_[i] = alpha*alpha*a_[i];
		b_[i] = alpha*b_[i];
		c_[i] = alpha*alpha*c_[i];
	}
	// Bilinear Transformation
	const int N = n;
	const int half = N/2;
	
    
    // biquadFilterCascade
    std::vector<double> a_two_pole, b_two_pole;
    double A, B, C, a, b, c;
    const double delta = M_PI/N;
    for (i = 0; i < half; i++)
    {
        a_two_pole.clear();
        b_two_pole.clear();
        
        // Numerator: A*s*s + B*s + C
        A = 1.0;
        B = 0.0;
        C = a_[i];
        bilinearTransform(filter_type, A, B, C, a, b, c);
        double b0 = a;
        double b1 = b;
        double b2 = c;
        
        // Denominator: A*s*s + B*s + C
        A = 1.0;
        B = b_[i];
        C = c_[i];
        bilinearTransform(filter_type, A, B, C, a, b, c);
        double a1 = b/a;
        double a2 = c/a;
        //
        b0 /= a;
        b1 /= a;
        b2 /= a;
        
        a_two_pole.push_back(0.);
        a_two_pole.push_back(-a1);        
        a_two_pole.push_back(-a2);
        
        // b0 is not useful in recursive filter design, since it would be attached to an unknown value
        // For readability, we push a 0. to the 0th location
        
        double sum = 0.;// a1 + a2 + b0 + b1 + b2;
        for(double num : b_two_pole)
        {
            sum += num;
        }
        for(double num : a_two_pole)
        {
            sum += num;
        }

        if(i == 0)
        {   // Incorporate k0 in first section
            b_two_pole.push_back(b0 * k0); 
            b_two_pole.push_back(b1 * k0);
            b_two_pole.push_back(b2 * k0);
        } else 
        {
            b_two_pole.push_back(b0); 
            b_two_pole.push_back(b1);
            b_two_pole.push_back(b2);
        }
        
#if FILTER_FILE_VERBOSE
        std::cout << "Sum = " << sum << "\n";
#endif
        biquadFilterCascade.emplace_back(std::make_unique<RecursiveFilter>(b_two_pole,a_two_pole, false));
    }
    
#if FILTER_FILE_VERBOSE
    const int size = biquadFilterCascade.size();
	for(std::unique_ptr<RecursiveFilter>& biQuad : biquadFilterCascade)
	{
        for(size_t j = 0; j < biQuad->B.size(); j++)
        {
            std::cout << "b" << j << " = " << biQuad->B[j] << '\n';
        }
        for(size_t j = 0; j < biQuad->A.size(); j++)
        {
            std::cout << "a" << j << " = " << biQuad->A[j] << '\n';
        }
		std::cout << "____________________"  << std::endl;
	}
#endif  
    
    delete[] X;
    delete[] Y;
    delete[] a_;
    delete[] b_;
    delete[] c_;
    
    return(true);
}


std::vector<double> getChebyshevBiQuads(double frequency_cutoff, 
    RecursiveFilter::filterType filter_type, double percent_ripple, int numPoles, int p)
/* Returns [a0, a1, a2, b1, b2] in a vector
 * This will only need to be run once, at the beginning of the program
 * These gains can then be passed into the filter, which should be able to
 * handle the rest

 * A note on getChebyshevBiQuads(...) & makeChebyshevFilters(...) 
 * These two functions were adapted from Visual Basic code written by Steven W Smith, which
 * can be found at http://www.dspguide.com/ch20/2.htm
 * or alternatively https://web.archive.org/web/20211118133224/http://www.dspguide.com/ch20/2.htm
 * These functions are used to calculate Chebyshev gains for a recursive IIR
 * filter (low-pass or high-pass).
 * Chebyshev filters add ripple (bad) but add steepness to the frequency response around the cutoff (good)
 * Since they are IIR filters, they 
 * We want to attenuate as much noise as possible in the stop band,
 * while prioritising smoothness and low latency, so the give in this equation is 
 * the presence of some ripple, which we can control by setting appropriate gains.
 * 
 * @param frequency_cutoff      
 *      Expects inputs between 0. to 0.5
 *      Frequency cutoff is relative to the sampling frequency, so make sure to account for this
 *      before passing it in
 *
 * @param filter_type 
 *      Either RecursiveFilter::low_pass or RecursiveFilter::high_pass
 * 
 * @param percent_ripple 
 *      Total ripple allowed in the passband
 *
 * @param number_of_poles 
 *      Even integers only  (2,4,...20)
 * 
 * @param p 
 *      The index of the biquad (from 0 to total_biquads-1)
 *
 */
{
	// Calculate the pole location on the unit circle
	double pole_position_real = -cos(M_PI/(numPoles*2.) + (p-1) * M_PI/numPoles);
	double pole_position_imag = sin(M_PI/(numPoles*2.) + (p-1) * M_PI/numPoles);	

	// Warp from a circle to an ellipse
	if(percent_ripple > 0)
	{
		double es = sqrt(pow(100./(100.-percent_ripple),2.) - 1.);
		double vx = (1./numPoles)* log((1./es)+sqrt((1./(es*es)+1.)));
		double kx = (1./numPoles)* log((1./es)+sqrt((1./(es*es)-1.)));
		kx = (exp(kx) + exp(-kx))*0.5;
		pole_position_real = pole_position_real * ((exp(vx) - exp(-vx))*0.5) / kx;
		pole_position_imag = pole_position_imag * ((exp(vx) + exp(-vx))*0.5 ) /kx;

#if FILTER_FILE_VERBOSE
        std::cout << '\n';
        std::cout << "================" << '\n';
        std::cout << "P\t=\t" << p << '\n';
        
        std::cout << "RP\t=\t" << pole_position_real << '\n';
        std::cout << "IP\t=\t" << pole_position_imag << '\n';
		std::cout << "ES\t=\t" << es << '\n';
		std::cout << "VX\t=\t" << vx << '\n';
		std::cout << "KX\t=\t" << kx << '\n';
#endif
	} else
	{
#if FILTER_FILE_VERBOSE
        std::cout << '\n';
        std::cout << "================" << '\n';
        std::cout << "P\t=\t" << p << '\n';
        
        std::cout << "RP\t=\t" << pole_position_real << '\n';
        std::cout << "IP\t=\t" << pole_position_imag << '\n';
		std::cout << "ES\t=\t" << "not used" << '\n';
		std::cout << "VX\t=\t" << "not used" << '\n';
		std::cout << "KX\t=\t" << "not used" << '\n';
#endif
	}

	// s domain to z domain conversion
	double t = 2.*tan(0.5);
	double w = 2.*M_PI*frequency_cutoff;
	double m = pole_position_real*pole_position_real + pole_position_imag*pole_position_imag;
	double d = 4.-4.*pole_position_real*t + m*t*t;
	double x0 = t*t/d;
	double x1 = 2.*t*t/d;
	double x2 = t*t/d;
	double y1 = (8. - 2.*m*t*t)/d;
	double y2 = (-4. -4.*pole_position_real*t - m*t*t)/d;

#if FILTER_FILE_VERBOSE
	std::cout << '\n';
	std::cout << "T\t=\t" << t << '\n';
	std::cout << "W\t=\t" << w << '\n';
	std::cout << "M\t=\t" << m << '\n';
	std::cout << "D\t=\t" << d << '\n';
	std::cout << "X0\t=\t" << x0 << '\n';
	std::cout << "X1\t=\t" << x1 << '\n';
	std::cout << "X2\t=\t" << x2 << '\n';
	std::cout << "Y1\t=\t" << y1 << '\n';
	std::cout << "Y2\t=\t" << y2 << '\n';	
#endif


	double k;
	// Low pass to low pass, or low pass to high pass transform
    switch(filter_type)
    {
    case RecursiveFilter::low_pass:
		k = sin(0.5 - w*0.5) / sin(0.5 + w*0.5);
        break;
    case RecursiveFilter::high_pass:
        k = -cos(w*0.5 + 0.5) / cos(w*0.5 - 0.5);
        break;
    default:
        break;
    }
    
	d = 1 + y1*k - y2*k*k;
	double a0 = (x0 - x1*k + x2*k*k)/d;
	double a1 = (-2.*x0*k + x1 + x1*k*k - 2.*x2*k)/d;
	double a2 = (x0*k*k - x1*k + x2)/d;
	double b1 = (2.*k + y1 + y1*k*k - 2.*y2*k)/d;
	double b2 = (-k*k - y1*k + y2)/d;
    
	if(filter_type == RecursiveFilter::high_pass)
	{
		a1 = -a1;
		b1 = -b1;
	}
    
#if FILTER_FILE_VERBOSE
	std::cout << '\n';
	// Return a0, a1, a2, b1, b2
    
    std::cout << '\n';

    std::cout << "A" << 0 << ", " << a0 << '\n';
    std::cout << "A" << 1 << ", " << a1 << '\n';
    std::cout << "A" << 2 << ", " << a2 << '\n';
    
    std::cout << "B" << 1 << ", " << b1 << '\n';
    std::cout << "B" << 2 << ", " << b2 << '\n';
#endif
    
    std::vector<double> output;
    output.push_back(a0);
    output.push_back(a1);
    output.push_back(a2);
    output.push_back(b1);
    output.push_back(b2);
    return(output);
}



//-----------------------------------------------------------------------------
void makeChebyshevFilters(double frequency_cutoff, RecursiveFilter::filterType filter_type, 
	double percent_ripple, int number_of_poles, 
    RecursiveFilter& singleFilter,
    std::vector< std::unique_ptr<RecursiveFilter> >* biquadFilterCascade)
/*
 * Construct a Chebysheve Filter
 * A note on getChebyshevBiQuads(...) & makeChebyshevFilters(...) 
 * These two functions were adapted from Visual Basic code written by Steven W Smith, which
 * can be found at http://www.dspguide.com/ch20/2.htm
 * or alternatively https://web.archive.org/web/20211118133224/http://www.dspguide.com/ch20/2.htm
 * These functions are used to calculate Chebyshev gains for a recursive IIR
 * filter (low-pass or high-pass).
 * Chebyshev filters add ripple (bad) but add steepness to the frequency response around the cutoff (good)
 * Since they are IIR filters, they 
 * We want to attenuate as much noise as possible in the stop band,
 * while prioritising smoothness and low latency, so the give in this equation is 
 * the presence of some ripple, which we can control by setting appropriate gains.
 *
 * @param frequency_cutoff      
 *      Expects inputs between 0. to 0.5
 *      Frequency cutoff is relative to the sampling frequency, so make sure to account for this
 *      before passing it in
 *
 * @param filter_type 
 *      Either RecursiveFilter::low_pass or RecursiveFilter::high_pass
 *
 * @param percent_ripple 
 *      0% to 29% (e.g. 1. -> 1%)
 *
 * @param number_of_poles 
 *      Even integers only  (2,4,...20)
 *
 * @param singleFilter
 *      Container for the output filter
 *
 * @param biquadFilterCascade
 *      Container for a biquadFilterCascade version of the output filter (optional)      
 *
 */
{
    
    
    std::vector<double> a_full, b_full, a_two_pole, b_two_pole;
    
    
    if(number_of_poles > 20)
    {
        std::cerr << "getChebshevGains requested with 20 poles, which is unsupported, run with 20 or fewer" << '\n';
        exit(-1);
    }
    if(number_of_poles %2 == 1)
    {
        std::cerr << "getChebshevGains requested with an non-even number of poles, aborting" << '\n';
        exit(-1);
    }
    
	const int n = 22;

	double a[n];	// Holds the 'a' coefficients upon program completion
	double b[n];
	double t_a[n];	// Internal use for combining stages
	double t_b[n];	// Internal use for combining stages

    // Initialising gains
	std::fill_n(&a[0], 22, 0.);
	std::fill_n(&b[0], 22, 0.);

	a[2] = 1;
	b[2] = 1;

    if(biquadFilterCascade != nullptr)
    {
        biquadFilterCascade->clear();
    }
    // Accumulating two pole gains to get full gains
	for(int p = 1; p < number_of_poles/2+1; p++)
	{
        a_two_pole.clear();
        b_two_pole.clear();
        
		std::vector<double> ab = getChebyshevBiQuads(frequency_cutoff, filter_type, percent_ripple, number_of_poles, p);
        
        double a0 = ab[0];
        double a1 = ab[1];
        double a2 = ab[2];
        double b1 = ab[3];
        double b2 = ab[4];
        

        a_two_pole.push_back(a0);
        a_two_pole.push_back(a1);        
        a_two_pole.push_back(a2);
        // b0 is not useful in recursive filter design, since it would be attached to an unknown value
        // For readability, we push a 0. to the 0th location
        b_two_pole.push_back(0.); 
        b_two_pole.push_back(b1);
        b_two_pole.push_back(b2);
        
        if(biquadFilterCascade != nullptr)
        {   
            biquadFilterCascade->emplace_back(std::make_unique<RecursiveFilter>(a_two_pole, b_two_pole, true));
        }

		for(int i = 0; i < n; i++)
		{
			t_a[i] = a[i];
			t_b[i] = b[i];
		}
		for(int i = 2; i < n; i++)
		{
			a[i] = a0*t_a[i] + a1*t_a[i-1] + a2*t_a[i-2];
			b[i] = t_b[i] - b1*t_b[i-1] - b2*t_b[i-2];
		}
	}

    // Finish combining coefficients
	b[2] = 0;
	for(int i = 0; i < n-2; i++)
	{
		a[i] = a[i+2];
		b[i] = -b[i+2];
	}

    // Normalize the gain
	double s_a = 0;
	double s_b = 0;

	for(int i = 0; i < n-2; i++)
	{
        switch(filter_type)
        {
        case RecursiveFilter::low_pass:
            s_a = s_a + a[i];
			s_b = s_b + b[i];
            break;
        case RecursiveFilter::high_pass:
			s_a = s_a + a[i] * pow(-1.,i);
			s_b = s_b + b[i] * pow(-1.,i);
            break;
        default:
            // unsupported
            break;
        }
	}
	double gain = s_a / ( 1. - s_b);

	for(int i = 0; i < n-2; i++)
	{
		a[i] = a[i]/gain;
	}
    
#if FILTER_FILE_VERBOSE
    std::cout << '\n';
#endif
    for(int i = 0; i < number_of_poles+1; i++)
    {
#if FILTER_FILE_VERBOSE
        std::cout << "A" << i << ", " << a[i] << '\n';
#endif 
        a_full.push_back(a[i]);
    }
    for(int i = 0; i < number_of_poles+1; i++)
    {
        if(i != 0)
        {
#if FILTER_FILE_VERBOSE
            std::cout << "B" << i << ", " << b[i] << '\n';
#endif
            b_full.push_back(b[i]);
        } else
        {
            b_full.push_back(0.);
        }
    }
    singleFilter.setFilter(a_full, b_full);
}


//-----------------------------------------------------------------------------
void makeAlphaBetaFilter(double frequency_cutoff, RecursiveFilter& inFilter)
/*
 * Construct an Alpha Beta Filter
 *
 * @param frequency_cutoff 
 *      Frequency cutoff is relative to the sampling frequency, so make sure to account for this
 *      before passing it in
 *
 * @param inFilter 
 *      The container filter
 */
{
    double alpha = (2.*M_PI*frequency_cutoff)/(2.*M_PI*frequency_cutoff + 1);
    std::vector<double> a,b;
    a.push_back(alpha);
    a.push_back(0.);
    
    b.push_back(0.);
    b.push_back(1.-alpha);
    
    inFilter.setFilter(a, b);
}


//-----------------------------------------------------------------------------
FilterZeroCrossingStats::FilterZeroCrossingStats()
/*
 * Default constructor for FilterZeroCrossingStats
 */
{
    lastZeroCrossing = -1.;
}


FilterZeroCrossingStats::FilterZeroCrossingStats(std::string mTag)
/*
 * Constructor for FilterZeroCrossingStats
 *
 * @param mTag 
 *      A name tag attached to this statistic, e.g. the name of the filter
 */
{
    tag = mTag;
    lastZeroCrossing = -1.;
}


void FilterZeroCrossingStats::update(double t, double filterOutput, 
        std::ofstream& latencyOutputFile, double lastInputZeroCrossing,
        bool isFilterActive)
/*
 * Constructor for FilterZeroCrossingStats
 *
 * @param t 
 *      The current time
 *
 * @param filterOutput
 *      The output from the filter we are analysing
 *
 * @param latencyOutputFile
 *      The output file that serves as a log for latency values and zero crossings
 *
 * @param lastInputZeroCrossing
 *      The time of the last zero crossing for the raw unfiltered signal
 *
 * @param isFilterActive
 *      If the filter has been activated
 */
{
    if(isFilterActive)
    {
        if(isZeroCrossing(previousFilterOutput, filterOutput))
        {
            // Zero crossing point
            latencyOutputFile << "Zero crossing (" << tag << "): " << t << '\n';
            lastZeroCrossing = t;
            latencyReadings.push_back(lastZeroCrossing - lastInputZeroCrossing);
            latencyOutputFile << "Latency: " << latencyReadings.back()*1000. << "ms" << '\n';
        } 
    }        
    previousFilterOutput = filterOutput;
}


//-----------------------------------------------------------------------------
FIRFilter::FIRFilter()
/*
 * Empty stub FIR Filter constructor. This class exists mainly to be inherited from 
 * by specific FIR filter implementations (e.g. max filter, min filter, moving average filter)
 */
{
    setOrder(0);
}


void FIRFilter::setOrder(unsigned int order)
/*
 * Set the order  (length) of the FIR filter
 *
 * @param order
 *		The new order of the FIR filter
 *
 */
{
    N = order;
}


double FIRFilter::getOrder()
/*
 * Returns the order of the FIR filter
 *
 * @return
 *		The order (length) of the FIR filter
 *
 */
{
    return(N);
}


double FIRFilter::getCurrentOutput()
/*
 * Returns the last filtered output value for derived classes of FIR (Finite Impulse Response) Filters
 *
 * @return
 *		The current output (i.e. y[0])
 *
 */
{
    if(y.empty())
    {
        return(0.);
    } else 
    {
        return(y.front());
    }
}


double FIRFilter::update(double xNew)
/*
 * Generic stub implementation that shouldn't be called. Inheritors should override this function
 * with their own target behaviour.
 *
 * @param xNew
 *		The new input value
 *
 * @return
 *		Returns 0.
 *
 */
{
    // Default implementation. Shouldn't be called
    std::cerr << "FIRFilter default update(double xNew) called, FIR sub-class may not override this function\n";
    return(0.);
}


//-----------------------------------------------------------------------------
MaxFilter::MaxFilter()
/*
 * Constructor for MaxFilter. 
 * This default constructor produces a non-functional filter, call setOrder or use a different constructor
 *
 */
{
    setOrder(0);
}


MaxFilter::MaxFilter(unsigned int order)
/*
 * Constructor MaxFilter
 *
 * @param order
 *		The length of the filter (e.g. 10 means 10 samples are used to produce the result)
 *
 */
{
    setOrder(order);
}

  
double MaxFilter::update(double xNew)
/*
 * Updates the state of the maximum filter
 *
 * @param xNew
 *		The new input value
 *
 * @return
 *		Returns the new ouput value for convenience. This value is also stored internally,
 *      and can be retrieved with getCurrentOutput()
 */
{
    // We maintain only a single y value in the array,
    // Since we are dealing with a finite impulse response
    if(y.empty())
    {
        y.push_back(xNew);
    }
    if(x.empty())
	{
		y[0] = xNew;
	} else 
	{
		// If the most recent x is greater than the current max,
		// it becomes the new max
		if(xNew >= y[0])
		{
			y[0] = xNew;
		} else if(x.size() >= N) //N -> the length of the filter
		{
			// Once we add xNew to x, x.size() == N+1
			// As a result, we will need to remove the last element
			// of the filter, so we need to do the following.

			// 1) Check if the maximum value is leaving the filter
			// 2) If so, then we need to recalculate the maximum

			if(x.back() >= y[0])
			{
				double newMax = 0.;
				// Need to recalculate, but exclude the last element
				for(size_t i = 0; i < x.size()-1; i++)
				{
					if(i == 0 || newMax < x[i])
					{
						newMax = x[i];
					}	
				}
				if(xNew > newMax)
				{
					newMax = xNew;
				}
				y[0] = newMax;
			} // else, do nothing, as the max won't change
		}
	} 
	x.push_front(xNew);
	if(x.size() > N) 
	{
		x.pop_back();
	}
	return(y[0]);
}


//-----------------------------------------------------------------------------
MinFilter::MinFilter()
/*
 * Constructor for MinFilter. 
 * This default constructor produces a non-functional filter, call setOrder or use a different constructor
 *
 */
{
    setOrder(0);
}


MinFilter::MinFilter(unsigned int order)
/*
 * Constructor for MinFilter
 *
 * @param order
 *		The length of the filter (e.g. 10 means 10 samples are used to produce the result)
 *
 */
{
    setOrder(order);
}

        
double MinFilter::update(double xNew)
/*
 * Updates the state of the minimum filter
 *
 * @param xNew
 *		The new input value
 *
 * @return
 *		Returns the new ouput value for convenience. This value is also stored internally,
 *      and can be retrieved with getCurrentOutput()
 */
{
    // We maintain only a single y value in the array,
    // since we are dealing with a finite impulse response
    if(y.empty())
    {
        y.push_back(xNew);
    }
    if(x.empty())
	{
		y[0] = xNew;
	} else 
	{
		// If the most recent x is greater than the current max,
		// it becomes the new max
		if(xNew <= y[0])
		{
			y[0] = xNew;
		} else if(x.size() >= N) //N -> the length of the filter
		{
			// Once we add xNew to x, x.size() == N+1
			// As a result, we will need to remove the last element
			// of the filter, so we need to do the following.

			// 1) Check if the maximum value is leaving the filter
			// 2) If so, then we need to recalculate the maximum

			if(x.back() >= y[0])
			{
				double newMin = 0.;
				// Need to recalculate, but exclude the last element
				for(size_t i = 0; i < x.size()-1; i++)
				{
					if(i == 0 || x[i] < newMin)
					{
						newMin = x[i];
					}	
				}
				if(xNew < newMin)
				{
					newMin = xNew;
				}
				y[0] = newMin;
			} // else, do nothing, as the max won't change
		}
	} 
	x.push_front(xNew);
	if(x.size() > N) 
	{
		x.pop_back();
	}
	return(y[0]);
}


//-----------------------------------------------------------------------------
MovingAverageFilter::MovingAverageFilter()
/*
 * Constructor for MovingAverageFilter. 
 * This default constructor produces a non-functional filter, call setOrder or use a different constructor
 *
 */
{
    setOrder(0);
}


MovingAverageFilter::MovingAverageFilter(unsigned int order)
/*
 * Constructor for MovingAverageFilter
 *
 * @param order
 *		The length of the filter (e.g. 10 means 10 samples are averaged to produce the result)
 *
 */
{
    setOrder(order);
}


double MovingAverageFilter::update(double xNew)
/*
 * Updates the state of the moving average filter
 *
 * @param xNew
 *		The new input value
 *
 * @return
 *		Returns the new ouput value for convenience. This value is also stored internally,
 *      and can be retrieved with getCurrentOutput()
 */
{
    // Premise: for a MAF, y(0) = (x(0) + x(1) + ... + x(n) + x(n-1))/n
    // y(1) = (x(1) + x(2) + ... + x(n))/n
    // Substitution of the terms in y(1) gives us y(0) = (x(0) + y(1) - x(n))/n
    // This means that we don't have to constantly sum all of the terms,
    // which can be costly for a long filter
    if(N > 0)
    {
        if(y.empty() || x.empty())
        {
            x.push_front(xNew);
            if(x.size() > N) 
            {
                // e.g. filter is 10 items long, we have added a new x value
                // remove the extraneous back value
                x.pop_back();
            }
            
            y.push_front(xNew);
            return(xNew);
        } else 
        {
            x.push_front(xNew);
            double newY = y.front() + xNew;

            if(x.size() > N)
            {
                // e.g. filter is 10 items long, we have added a new x value
                // remove the extraneous back value and subtract it from y to
                // get the new sum
                newY -= x.back();
                x.pop_back();
            }

            y.push_front(newY);
            if(y.size() > N)
            { 
                y.pop_back();
            }
            // Because we store a sum, we need to average the output
            return(newY/static_cast<double>(x.size()));
        }
    } else 
    {
        std::cerr << "Called Moving Average Filter without setting an order, or setting 0\n";
        std::cerr << "Please either initialise with a non-zero order, or call setOrder(int...)\n";
        return(0.);
    }
}


//-----------------------------------------------------------------------------
RecursiveFilter::RecursiveFilter() : numPoles(0)
/*
 * Empty constructor for RecursiveFilter, which also sets the filter properties
 * Call setFilter before use, or use a different constructor
 * 
 */
{
}
    
    
RecursiveFilter::RecursiveFilter(std::vector<double> mA, std::vector<double> mB,
        bool normaliseInputGains)
/*
 * Constructor for RecursiveFilter, which also sets the filter properties
 *
 * @param mA
 *      The gains for A
 *
 * @param seconds_per_revolution
 *      The gains for B
 *
 * @param normaliseInputGains
 *      A flag to automatically normalise the gains on output s.t. output = scaling*(sum(mA) + sum(mB))
 *      evaluates to 1. This is achieved by calculating the sum and using it as the scaling factor
 */
{
    setFilter(mA, mB, normaliseInputGains);
}


void RecursiveFilter::setFilter(std::vector<double> mA, std::vector<double> mB,
    bool normaliseInputGains)
/*
 * A recursive filter represented by 
 * y(0) = A(0)*x() + A(-1)*x(-1) + ... + A(-N)*x(-N) + B(-1)*y(-1) + ... + B(-N)*y(-N)
 * Which is derived from its transfer function. Note that B(0) is not used, as it
 * is folded into the rest of the gain values
 *
 * @param mA
 *      The gains for A
 *
 * @param mB
 *      The gains for B
 *
 * @param normaliseInputGains
 *      A flag to automatically normalise the gains on output s.t. output = scaling*(sum(mA) + sum(mB))
 *      evaluates to 1. This is achieved by calculating the sum and using it as the scaling factor
 */
{
    numPoles = std::max(static_cast<unsigned int>(mA.size()), static_cast<unsigned int>(mB.size()));
    if(numPoles > 0)
    {
        numPoles = numPoles - 1;
    }
    if(numPoles < 0)
    {
        std::cerr << "setFilter called with negative numPoles, which is invalid.\n";
    }   
    A = mA;
    B = mB;
    
    if(normaliseInputGains)
    {
        scalingFactor = 0.;
        for(double gain: mA)
        {
            scalingFactor += gain;
        }
        for(double gain: mB)
        {
            scalingFactor += gain;
        }
    } else
    {
        scalingFactor = 1.;
    }
}


unsigned int RecursiveFilter::getNumPoles()
/*
 * Returns the number of poles in the filter. This is the same as the order of the filter.
 *
 * @return
 *		The number of poles (the filter order) as an unsigned int
 */
{
    return(numPoles);
}


double RecursiveFilter::getCurrentOutput()
/*
 * Returns the last updated output value of the filter
 *
 * @return
 *		Returns the ouput value that was calculated last time update(...) was called
 */
{
    if(y.empty())
    {
        return(0.);
    } else
    {
        return(y[0]);
    }
}
    

double RecursiveFilter::update(double xNew)
/*
 * Updates the state of the recursive filter
 *
 * @param xNew
 *		The new input value
 *
 * @return
 *		Returns the new ouput value for convenience. This value is also stored internally,
 *      and can be retrieved with getCurrentOutput()
 */
{
    if((B.size() == numPoles + 1) && (A.size() == numPoles + 1))
    {
        x.push_front(xNew);
        y.push_front(0);
        
        // To use an example, let's say we have a two pole filter (numPoles == 2)
        // y[0] = a0*x[0] + a1*x[-1] + a2*x[-2] + b1*y[-1] + b2*y[-2]
        // x needs to be greater than 2 in order for the filter to be operational
        // x doesn't need to be 
        if(x.size() > numPoles && y.size() > numPoles)
        {
            // Remove the oldest measurements if they
            // move outside the reach of the filter
            if(x.size() > numPoles +1)
            {
                x.pop_back();
            }
            if(y.size() > numPoles +1)
            {
                y.pop_back();
            }
         
            double y_new = 0;
            for(size_t i = 0; i < A.size(); i++)
            {
                y_new += A[i]*x[i];
            }
            // Ignore B0, because y[0] hasn't been calculaed yet
            for(size_t i = 1; i < B.size(); i++)
            {
                y_new += B[i]*y[i];
            }
            y[0] = (y_new)/scalingFactor;

            return(y[0]);
        } else
        {  
            // TO IMPLEMENT: PRE-ACTIVATION BEHAVIOUR
            // We may decide to apply a different rule before the filter activates
            // For example, 
            // 1) Use zero value
            // 2) Sample and hold the first value
            // 3) Sample and hold an average of the first two values
            // 4) Average the values in the x deque
            // 5) Median filter the values in the x deque 
            
            // Filter is not yet active
            return(0.);
        }
    } else
    {
        std::cerr << "ERROR: Filter update called with invalid filter gains set. Filter gains must be numPoles+1 in length.\n";
        std::cerr << "A and B gains must be equal in length\n";
        std::cerr << "Number of poles == " << numPoles << '\n';
        std::cerr << "A.size() == " << A.size() << '\n';
        std::cerr << "B.size() == " << B.size() << '\n';
        return(0.);
    }
    return(0.);
}