//-----------------------------------------------------------------------------
//
//  MidiSmoother.h
//  MidiSmoother
//
//  Created by Nathan Holmberg on 9/04/14.
//
//  Edited by Ben O'Brien on 18/02/22
//
//-----------------------------------------------------------------------------

#ifndef MidiSmoother_MidiSmoother_h
#define MidiSmoother_MidiSmoother_h

#include <chrono>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include "filters.h"


class MidiSmoother
{
public:
	MidiSmoother( int midi_values_per_revolution, 
            double seconds_per_revolution, 
            double audio_samples_per_block,
            double audio_sample_rate);
                
    virtual ~MidiSmoother();
	
	void NotifyMidiValue( char midi_value );
	
	double RequestMSToMoveValue( double ms_to_process ); // removed const modifier
	
	void StartMidiProcessing();
	
	void StopMidiProcessing();
	
	bool MidiIsProcessing() const;
    
    void WaitForCompletion();
private:

    //-------------------------------------------------------------------------
    // We receive data asynchronously and at irregular intervals
    // The data consists of a noisy sequence of values representing position deltas
    // Since we are looking to calculate velocity, we must divide the position deltas
    // by the measured interval between the current and previous message
    // This introduces additional noise, but also creates a step-wise
    // motion in the output, as the velocity is only updated when we receive
    // an event (often, the polling is more frequent)
    std::thread	mFilteringThread;
    std::mutex mFilteringThreadStartMutex;
    std::condition_variable mFilteringThreadStart;
    
    bool isFilteringThreadRunning;
    void StartFilteringThread();
    void StopFilteringThread();
    void FilteringThreadFunction();

    //-------------------------------------------------------------------------
	// These variables should not be modified, to ensure things continue as necessary

	// the number of midi values that would need to be recieved 
    // for an entire platter revolution to be expected
    const int mMidiValuesPerRevolution; 
    // the number of seconds an entire platter revolution represents
	const double mSecondsPerRevolution; 
    //-------------------------------------------------------------------------
    
    // filters don't trigger until at least one message has been recieved
    bool mHasNotReceivedAnyMidiMessages; 
    bool mbMidiIsProcessing;
    
    //-------------------------------------------------------------------------
    // FILTERS
    //-------------------------------------------------------------------------
    // Pre-screening max and min filters determine the noise envelope, which
    // is then averaged to get a step-wise filtered value
    MaxFilter mMaxFilter;
    MinFilter mMinFilter;   
    
    // This is then fed into a chebyshev filter to smooth the output
    // This two step process produces smoother results than just one
    // Chebyshev filter, even when accounting for the less aggressive
    // attenuation needed to attain decent latency
    RecursiveFilter mLowPassChebyshevFilter;
    
    //-------------------------------------------------------------------------
    // we can't guarantee that double read/write operations are natively atomic
    // on the specific architecture this program is run on, so make both of
    // the following raw and filtered velocity values atomic
    
    // updated whenever NotifyMidiValue is called
	std::atomic<double> mLastVelocityRaw;
    //mLastVelocitySmoothed - updated at set intervals within the filtering thread
	std::atomic<double> mLastVelocityFiltered;
    
    //-------------------------------------------------------------------------
    // Variables used to track the time intervals between the previous and most recent MIDI events
	std::chrono::steady_clock::time_point mTimeOfLastNotifyMidiValue;
    
    // The last interval between two midi events that was non zero
    // -ve numbers are used to indicate an unitialised value,
    // as they would otherwise not show up in the wild unless something
    // has gone wrong with chrono
    long long int mLastNonZeroIntervalCount;
    
	// The rate at which the smoothing thread updates in microseconds
    double mSmoothingFilterThreadUpdateInterval_us;
    
};

#endif
