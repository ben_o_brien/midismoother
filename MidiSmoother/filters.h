//-----------------------------------------------------------------------------
//
//  Filters.h
//  MidiSmoother
//
//  Created by Ben O'Brien on 18/02/22
//
//-----------------------------------------------------------------------------


#ifndef __MidiSmoother__Filters__H
#define __MidiSmoother__Filters__H

#include <deque>
#include <vector>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <memory>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <fstream>


template <typename T> int sgn(T val) 
/*
 * Returns the sign of an input number
 *
 * @param val
 *		The number we want to check
 *
 * @return
 *		-1 if -ve, 1 if +ve
 */
{
    return (T(0) < val) - (val < T(0));
}


template <typename T> bool isZeroCrossing(T a, T b) 
/*
 * Determines if the progression from a to b represents a zero crossing
 *
 * @param a
 *
 * @param b
 *
 * @return
 *		true if it represents a zero crossing
 */
{
    if(a <= 0 && b >= 0)
    {
        return(true);
    } else if(a >= 0 && b <= 0)
    {
        return(true);
    } else 
    {   
        return (false);
    }
}


class FIRFilter
{
public:
    FIRFilter();
    
    void setOrder(unsigned int order);
    double getOrder();
    
    virtual double update(double xNew);
    double getCurrentOutput();
protected:
    // The length of the filter
    unsigned int N;
    
    // The backlog of input values 
    std::deque<double> x; 
    

    // The backlog of output values - used for optimisation purposes
    // Although in theory you don't need the output backlog, for a finite impulse response
    // it allows for certain optimisations (e.g. for moving average filters)
    std::deque<double> y;
};


// Classes derived from FIR filter
class MaxFilter : public FIRFilter
{
public:
    MaxFilter();
    MaxFilter(unsigned int order);

    double update(double xNew);
};

class MinFilter : public FIRFilter
{
public:
    MinFilter();
    MinFilter(unsigned int order);
    
    double update(double xNew);
};

class MovingAverageFilter : public FIRFilter
{
public:
    MovingAverageFilter();
    MovingAverageFilter(unsigned int order);
    
    double update(double xNew);
};


// A generic recursive filter
class RecursiveFilter
{
public:
    RecursiveFilter();
    RecursiveFilter(std::vector<double> mA, std::vector<double> mB,
        bool normaliseInputGains = false);
    
    //--------------------------------------------------------------------------------------
    // mA: A0, A1, ... , AN, where N is num_poles
    // mB: B0, B1, ... , BN, where N is num_poles
    // Note that B0 is unused
    void setFilter(std::vector<double> mA, std::vector<double> mB, bool normaliseInputGains = false);
    
    // Recalculates y and returns it
    // the new y is stored at the end of the array
    double update(double newX);
    double getCurrentOutput();
    
    unsigned int getNumPoles();
    
    std::vector<double> A;
    std::vector<double> B;
    
    //--------------------------------------------------------------------------------------
public: // Scoped enums
    enum filterType { low_pass = 0, high_pass = 1};
private:    
    unsigned int numPoles;
    
    // Sometimes recursive filters (e.g. MAF) come out without normalisation,
    // but correcting this internally would destroy the filter's characeristics
    // We store a scaling factor, the sum of all of the gains, to account for this
    double scalingFactor;  
    
    // The backlog of input values 
    std::deque<double> x; 
    // The backlog of output values
    std::deque<double> y; 
};


// Object to store statistics, used to estimate filter latency via zero crossings
class FilterZeroCrossingStats
{
public:
    FilterZeroCrossingStats();
    FilterZeroCrossingStats(std::string mTag);
    
    void update(double t, double filterOutput, std::ofstream& latencyOutputFile, 
            double lastInputZeroCrossing, bool isFilterActive);
    
    //-------------------------------------------------------------------------
    double previousFilterOutput;
    std::vector<double> latencyReadings;
    double lastZeroCrossing;
    std::string tag;
};


void makeAlphaBetaFilter(double frequency_cutoff, RecursiveFilter& inFilter);


// BiquadFilterCascade is not constructed unless you pass in a container
void makeChebyshevFilters(double frequency_cutoff, RecursiveFilter::filterType filter_type, 
	double percent_ripple, int number_of_poles, 
    RecursiveFilter& singleFilter,
    std::vector< std::unique_ptr<RecursiveFilter> >* biquadFilterCascade = nullptr);


bool makeEllipticFilterCascade(RecursiveFilter::filterType filter_type, 
    double cutFreq_rads, 
    double passBand_dB, 
    double stopFreq_rads, 
    double stopBand_dB, 
    std::vector< std::unique_ptr<RecursiveFilter> >& biquadFilterCascade);
    
    
#endif